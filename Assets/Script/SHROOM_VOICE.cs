﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]

public class SHROOM_VOICE : MonoBehaviour {

  public static SHROOM_VOICE me { get; private set; }

  private AudioSource asSound;

  void Awake() {
    if (me != null && me != this) {
      Destroy(gameObject);        
    } else {
      me = this;// Here we save our singleton instance
      GameObject.DontDestroyOnLoad(this.gameObject);
    }
  }
  

  void Start() {
    asSound = GetComponent<AudioSource>();
    //иногда бывает баг с 2д звуками. позиция источника звука решает эту проблему
    GetComponent<Transform>().position = Camera.main.GetComponent<Transform>().position;

  }

  public void PlaySoundOnce( AudioClip a, float v = 1 ) {
    asSound.Stop();
    asSound.PlayOneShot(a,v);
  }

  public void SoundOFF() {
    PlayerPrefs.SetInt("s", 0);
    asSound.volume = 0.0F;
  }

  public void SoundON() {
    PlayerPrefs.SetInt("s", 1);
    asSound.volume = 1.0F;
  }
  
}
