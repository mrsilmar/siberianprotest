﻿using UnityEngine;
using System.Collections;

public class ConcreteShroom : AbstraktShroom
{

  public int pressCount; 
  public Sprite[] spHit;
  public AudioClip[] audHit;


  private int _currentPressCount;
  private Sprite spDefault;

  protected override void Hit() {
    if (currentHealth > 0) {  
      --currentHealth;
      sr.sprite = spHit[currentHealth];
   
    } else {
      currentHealth = Health;
      sr.sprite = spDefault;
    }
   
  
  }

  protected override void Prepare() {
    spDefault = sr.sprite;
    _currentPressCount = pressCount;
  }

  protected override void PushEvent()
  {
    SHROOM_VOICE.me.PlaySoundOnce(audHit[_currentPressCount-1], 0.95F);
    if (_currentPressCount > 1){
      --_currentPressCount;    
    } else{ 
      Hit();
      _currentPressCount = pressCount;
    }
   

  }

}
