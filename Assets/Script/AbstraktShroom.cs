﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animation))]
[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(BoxCollider2D))]

public class AbstraktShroom : MonoBehaviour {

  public int Health;


  protected SpriteRenderer sr;
  protected Animation ani;
  protected int currentHealth;

  private float _length;
  private bool _isHoldingEnded;
  // Use this for initialization

  protected virtual void Prepare() {
    Debug.Log("Meet the " + this.gameObject.name + ": 'I am usual shroom hohoha!'");
  }


  protected virtual void PushEvent() {
   Debug.Log ("I am Parento PushEvent() method. Sorry but i have no any implementation.. =(");
  }

  protected virtual void Hit() {
    Debug.Log("I am Parento Hit() method. ");
  }


  private void Start () {
    currentHealth = Health;

    sr = GetComponent<SpriteRenderer>();

    ani = GetComponent<Animation>();  
    ani.AddClip(ani.clip, "push", 0, 5, false);
    ani.AddClip(ani.clip, "release", 6, 15, false);
    ani["push"].wrapMode = WrapMode.Once;
    ani["release"].wrapMode = WrapMode.Once;
    _length = ( 15 - 6 ) / ani.clip.frameRate;

    _isHoldingEnded = true;



    Prepare();
  }


  private IEnumerator WaitForAnimation( Animation animation ) {
    do {
      yield return null;
    } while (animation.isPlaying);
  }

  private void StartAndWaitAnimation( string animationName ) {
    StartCoroutine(PlayAnimationAsync(animationName));
  }

  private IEnumerator PlayAnimationAsync( string animationName ) {

    ani.Play(animationName);
    yield return new WaitForSeconds(_length);                                             
 
  }


  private void OnMouseDrag() {
    if (!_isHoldingEnded) return;
      _isHoldingEnded = false;
      ani.Play("push");
     PushEvent();
  }

  private void OnMouseUp() {
    _isHoldingEnded = true;
    StartAndWaitAnimation("release");
    ani.Play("release");
  }



}
