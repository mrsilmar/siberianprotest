﻿using UnityEngine;
using System.Collections;

public class SpriteResizer : MonoBehaviour {

  private float _worldScreenWidth;
	private float _worldScreenHeight;

	void Start () {	
		_worldScreenHeight=Camera.main.orthographicSize*2f;
		_worldScreenWidth=_worldScreenHeight/Screen.height*Screen.width;
	  ResizeWidth(GetComponent<SpriteRenderer>());
				
	}
  
	void ResizeWidth(SpriteRenderer sr){
		float scale = _worldScreenWidth / sr.sprite.bounds.size.x;
		sr.transform.localScale = new Vector3(scale,scale,1);
	}
  
  
}
